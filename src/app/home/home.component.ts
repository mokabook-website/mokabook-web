import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../services/auth.service';
import { CommonService } from '../services/common.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit ,AfterViewInit
{
homeData:any;
image:any;
bannerData:any;
categoryData:any;
featuredData:any;
bestsellerData:any;
offerData:any;
changecolor:boolean = false;
favicon:any;
recommendedData:any;
googleid:any;
profiledata:any;
googledata:any;
bannerDataVal:any[]=[];
categoryDataVal:any[]=[];
featuredDataVal:any[]=[];
bestsellerDataVal:any[]=[];
bestseller:any[]=[];
offerDataVal:any[]=[];
recommendedDataVal:any[]=[];
slideConfig = { slidesToShow: 1, slidesToScroll: 1,autoplay: true,
autoplaySpeed: 3000,dots:true};
isloggin:any;
imgurl:any;

  constructor( private router: Router,
    public authenticationService: AuthenticationService,
    private commonService: CommonService,
    private _route: ActivatedRoute,private spinner: NgxSpinnerService) 
    {

     
      this.isloggin = localStorage.getItem("sucess");
      // this.authenticationService.googlefbget.subscribe((res:any)=>
      // {
      //   this.googleid = res.id;
      //   localStorage.setItem("googleid",this.googleid);
      //   console.log("googledata",res);
      // });
      this.googledata =  localStorage.getItem("googledata");

      if(localStorage.getItem('sucess') && this.googleid == null)
      {
        
        this.authenticationService.getprofile().subscribe((profdata: any) => {
        
          this.profiledata = profdata.data._id;
          localStorage.setItem("userid",this.profiledata);
        //  console.log("this.profile",this.profiledata);
        });
      }
    

}

  ngOnInit(){
    this.imgurl=this.authenticationService.imageUrl;
    this.favicon = 'assets/images/like.png';
    this.getHomedata();
   
  }

  ngAfterViewInit()
  {
    this.spinner.show();
  }

  gotoproductDetails(id){

    this.router.navigate(['productdetail'], { queryParams: { 'id': id }});
    
  }

  getHomedata()
  {
    var id = localStorage.getItem("userid")
   
  if(localStorage.getItem('sucess') && localStorage.getItem('userid') && this.googleid == null)
    {
  this.authenticationService.getfavdashboard(id).subscribe((res:any)=>{
 
  if(res.success){

this.homeData=res.data.output;
this.bannerData=this.homeData.filter(ij=>ij.label=="Banner")[0];
//console.log("banner",this.bannerData);

if(this.bannerData!==null && this.bannerData!==undefined){
this.bannerDataVal=this.bannerData.value;
//console.log("this.bannerdataval",this.bannerDataVal);

}
this.categoryData=this.homeData.filter(ij=>ij.label=="Shop By Category")[0];

if(this.categoryData!==null && this.categoryData!==undefined){
  this.categoryDataVal=this.categoryData.value;
  
  }
this.featuredData=this.homeData.filter(ij=>ij.label=="Featured")[0];

if(this.featuredData!==null && this.featuredData!==undefined){
  this.featuredDataVal=this.featuredData.value;
 // console.log("feature",this.featuredDataVal);
  }


this.bestsellerData=this.homeData.filter(ij=>ij.label=="Best Seller")[0];

if(this.bestsellerData!==null && this.bestsellerData!==undefined){
  this.bestsellerDataVal=this.bestsellerData.value;
  }

this.offerData=this.homeData.filter(ij=>ij.label=="Offers")[0];

if(this.offerData!==null && this.offerData!==undefined){
  this.offerDataVal=this.offerData.value;
  
  }

this.recommendedData=this.homeData.filter(ij=>ij.label=="Recommended")[0];
if(this.recommendedData!==null && this.recommendedData!==undefined){
  this.recommendedDataVal=this.recommendedData.value;
        this.image =   this.recommendedDataVal[0].images[0].image;
        //console.log(this.image)
  }

  }

})
  }
else
{
  this.authenticationService.getDashboard().subscribe((res:any)=>{
   // console.log("home data is", res.data );
 
    if(res.success){
  
  this.homeData=res.data.output;
  this.bannerData=this.homeData.filter(ij=>ij.label=="Banner")[0];
  //console.log("banner",this.bannerData);
  
  if(this.bannerData!==null && this.bannerData!==undefined){
  this.bannerDataVal=this.bannerData.value;
  
  }
  this.categoryData=this.homeData.filter(ij=>ij.label=="Shop By Category")[0];
  
  if(this.categoryData!==null && this.categoryData!==undefined){
    this.categoryDataVal=this.categoryData.value;
    
    }
  this.featuredData=this.homeData.filter(ij=>ij.label=="Featured")[0];
  
  if(this.featuredData!==null && this.featuredData!==undefined){
    this.featuredDataVal=this.featuredData.value;
   // console.log("feature",this.featuredDataVal);
    }
  
  
  this.bestsellerData=this.homeData.filter(ij=>ij.label=="Best Seller")[0];
  
  if(this.bestsellerData!==null && this.bestsellerData!==undefined){
    this.bestsellerDataVal=this.bestsellerData.value;
    }
  
  this.offerData=this.homeData.filter(ij=>ij.label=="Offers")[0];
  
  if(this.offerData!==null && this.offerData!==undefined){
    this.offerDataVal=this.offerData.value;
    
    }
  
  this.recommendedData=this.homeData.filter(ij=>ij.label=="Recommended")[0];
  if(this.recommendedData!==null && this.recommendedData!==undefined){
    this.recommendedDataVal=this.recommendedData.value;
          this.image =   this.recommendedDataVal[0].images[0].image;
         // console.log(this.image)
    }
  
    }
  
  })
}
  }
  goTocategoriesdetail(id) {
    this.router.navigate(['/categoriesdetail'], { queryParams: { 'id': id }});

  }
  goTocategories(value,name,data) {
    console.log("check",data);
    this.router.navigate(['/categories'],{queryParams: {'prod':name,'name':'moretoexplore','id':value}});
    // this.router.navigate(['/categories']);
  }

  goTosignup() {
    this.router.navigate(['/signup']);
  }

  goTologin() {
    this.router.navigate(['/login']);
  }

  addfavlist(id)
  {
   if(this.isloggin != null)
   {
    this.authenticationService.postfavlist(id).subscribe((res:any)=>
    {
      if(res.success)
      {
        this.changecolor = true;
        this.getfavlistdata()
      }
     // console.log("postfav",res);
    });
   }

  }


  getfavlistdata()
  {
    var id = localStorage.getItem("userid")
    this.authenticationService.getfavdashboard(id).subscribe((res:any)=>{
      if(res.success){
        this.homeData=res.data.output;
        this.bestsellerData=this.homeData.filter(ij=>ij.label=="Best Seller")[0];

        if(this.bestsellerData!==null && this.bestsellerData!==undefined){
          this.bestsellerDataVal=this.bestsellerData.value;
          }
          this.recommendedData=this.homeData.filter(ij=>ij.label=="Recommended")[0];
           if(this.recommendedData!==null && this.recommendedData!==undefined){
              this.recommendedDataVal=this.recommendedData.value;
              this.image =   this.recommendedDataVal[0].images[0].image;
          //    console.log(this.image)
  }
      }
    });
  }

  bannerpage(data)
  {

    data.type="bannerlist"
    //data.push(obj);
    this.authenticationService.getbannerdata(data);
  
      this.router.navigate(['/categories'],{ queryParams: { 'prod': 'bannerlist' , 'id':data._id }})
  
  }

  featurePage(data)
  {
    data.type = 'Feature'
    this.authenticationService.getbannerdata(data);
  
    this.router.navigate(['/categories'],{ queryParams: { 'prod': 'Feature' , 'id':data._id }})

  }
}
