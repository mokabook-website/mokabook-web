import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  footerdata:any; 
  subcatlist: any;
  name:string;
  termsvalue:any;
  constructor(private authservice:AuthenticationService,private route:Router,private router: ActivatedRoute) 
  { 

  }

  ngOnInit() 
  {
    this.router.queryParams.subscribe(params => { this.name = params['name'] }); 
    this.authservice.getDashboard().subscribe((res:any)=>{
      this.footerdata = res.data.output;
    
     this.footerdata=this.footerdata.filter(ij=>ij.label=="Shop By Category")[0].value;
  
     });

     this.authservice.getbannersubcat().subscribe((res:any)=>
     {
       this.subcatlist = res.data
     
     })
     this.authservice.gettermsandpolicy().subscribe((res)=>
     {
       this.termsvalue = res.data;
     })
  }


  gotoprodcategory(catid,subid,name)
  {
  //  "categories?id=5f0408bf01e0501ddef617fd&catid=5eb9151da49e9d6d7a988869&name=subproduct"
  if(this.name == 'subproduct')this.authservice.getfooterProd()
  this.route.navigate(['/categories'],{queryParams: {'id':subid,'catid':catid,'name':'subproduct','prod':name}})
  }

  termsPage(i)
  {
    this.authservice.gettermsvalue(i);
    this.route.navigate(['/terms&cond'],{queryParams: {check:i}})
  }
}
