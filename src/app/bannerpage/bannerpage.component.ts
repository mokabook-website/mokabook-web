import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-bannerpage',
  templateUrl: './bannerpage.component.html',
  styleUrls: ['./bannerpage.component.scss']
})
export class BannerpageComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }

  movehome()
  {
    this.router.navigate(['/home']);
  }
}
