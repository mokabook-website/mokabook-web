import { BrowserModule } from '@angular/platform-browser';
import { NgModule , CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { MaterialModule } from './material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {MatRadioModule} from '@angular/material/radio';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { VerficationComponent } from './verfication/verfication.component';
import { SignupComponent } from './signup/signup.component';
import { CategoriesComponent } from './categories/categories.component';
import { CategoriesdetailComponent } from './categoriesdetail/categoriesdetail.component';
import { ProfileComponent } from './profile/profile.component';
import { OrderComponent } from './order/order.component';
import { OrderdetailComponent } from './orderdetail/orderdetail.component';
import { LoginComponent } from './login/login.component';
import { ForgetpasswordComponent } from './forgetpassword/forgetpassword.component';
import { CartComponent } from './cart/cart.component';
import { ProductdetailComponent } from './productdetail/productdetail.component';
import { CheckoutComponent, DialogOverviewExampleDialog } from './checkout/checkout.component';
import { FooterComponent } from './footer/footer.component';
import { WishlistComponent } from './wishlist/wishlist.component';
import { AuthResponseInterceptor } from './services/auth.response.interceptor';
import { CommonService } from './services/common.service';
import { ToastrModule } from "ngx-toastr";

import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { NgxIntlTelInputModule } from "ngx-intl-tel-input";
import { ForgotPasswordEmailComponent } from './forgot-password-email/forgot-password-email.component';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import {MatDialogModule} from '@angular/material/dialog';
import {SocialLoginModule , AuthServiceConfig, GoogleLoginProvider , FacebookLoginProvider} from 'ng4-social-login';
import { DialogboxComponent } from './dialogbox/dialogbox.component';
import { CanceldialogboxComponent } from './canceldialogbox/canceldialogbox.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { BannerpageComponent } from './bannerpage/bannerpage.component';
import { JwSocialButtonsModule } from 'jw-angular-social-buttons';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { NgxStarRatingModule } from 'ngx-star-rating';
import {
  NgxSocialButtonModule,
  SocialServiceConfig
} from "ngx-social-button";
import { NotfoundPageComponent } from './notfound-page/notfound-page.component';
import { HeaderpageComponent } from './headerpage/headerpage.component';
import { Ng5SliderModule } from 'ng5-slider';
import { SubcategeoryComponent } from './subcategeory/subcategeory.component';
import { TermsComponent } from './terms/terms.component';
// Configs
export function getAuthServiceConfigs() {
  let config = new SocialServiceConfig()
      .addFacebook("255419545748628")
      .addGoogle("512748723625-aeuvojkfgc105kh3joveq7v3oc7dq0ma.apps.googleusercontent.com")
      .addLinkedIn("Your-LinkedIn-Client-Id");
  return config;
}

const config = new AuthServiceConfig([
{
id: GoogleLoginProvider.PROVIDER_ID,
provider: new GoogleLoginProvider('512748723625-aeuvojkfgc105kh3joveq7v3oc7dq0ma.apps.googleusercontent.com')
 //provider: new GoogleLoginProvider('512748723625-t0iegvfg3qr1o6hhfao6e9e0kothhknq.apps.googleusercontent.com')

},
{
  id: FacebookLoginProvider.PROVIDER_ID,
  provider: new FacebookLoginProvider('255419545748628')
}
],false)

export function provideConfig()
{
  return config;
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    VerficationComponent,
    SignupComponent,
    CategoriesComponent,
    CategoriesdetailComponent,
    ProfileComponent,
    OrderComponent,
    OrderdetailComponent,
    LoginComponent,
    ForgetpasswordComponent,
    CartComponent,
    ProductdetailComponent,
    DialogOverviewExampleDialog,
    CheckoutComponent,
    FooterComponent,
    WishlistComponent,
    ForgotPasswordEmailComponent,
    DialogboxComponent,
    CanceldialogboxComponent,
    BannerpageComponent,
    NotfoundPageComponent,
    HeaderpageComponent,
    SubcategeoryComponent,
    TermsComponent,

   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    MatDialogModule,
    MatRadioModule,
    MatProgressSpinnerModule,
    HttpClientModule,
    SlickCarouselModule,
    JwSocialButtonsModule,
    NgxSocialButtonModule,
    SocialLoginModule,
    BsDropdownModule.forRoot(),
    NgxIntlTelInputModule,
    MatPaginatorModule,
    NgxStarRatingModule,
    Ng5SliderModule,
    MatCheckboxModule,
    ToastrModule.forRoot({
      maxOpened: 1,
      autoDismiss: true
    }),
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  entryComponents: [DialogOverviewExampleDialog,CanceldialogboxComponent],
  providers: [CommonService, {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthResponseInterceptor,
    multi: true
  },{
   provide: AuthServiceConfig,
   useFactory: provideConfig
  },
  {
    provide: SocialServiceConfig,
    useFactory: getAuthServiceConfigs
}]
    ,
  bootstrap: [AppComponent]
})
export class AppModule { }
