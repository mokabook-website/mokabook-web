import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";

import {
  SearchCountryField,
  TooltipLabel,
  CountryISO
} from "ngx-intl-tel-input";
import { CommonService } from '../services/common.service';
import { AuthenticationService } from '../services/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  token = localStorage.getItem("token");
  title: string = "AGM project";
  latitude: number;
  longitude: number;
  message;
  zoom: number;
  ProfileForm: FormGroup;
  confirmError: boolean;
  public imagePath;
  imgURL: any;
  file: any;
  profilePic: any;
  counteryiso:string="CountryISO.India";

  CountryISO = CountryISO;
  preferredCountries: CountryISO[] = [
    CountryISO.UnitedStates,
    CountryISO.UnitedKingdom
  ];
  SearchCountryField = SearchCountryField;
  TooltipLabel = TooltipLabel;
  SentData:string;
  data : any;
  countryDataArray;
  submitted:boolean=false;
  viewIs;
  countryCodeData=CountryISO.India;
  constructor(private fb: FormBuilder,
    private router: Router,
    public authenticationService: AuthenticationService,
    private commonService: CommonService,
    private _route: ActivatedRoute) { }

  ngOnInit() {
    this.submitted=false;
    this.countryCodeData=CountryISO.India;
    this.createForm();
  }

  createForm() {
//     this.data = JSON.parse(localStorage.getItem('signUp'))
//     var valueData="";
//     var PhoneDataNumber="";
//     var PhoneDataCountry="";
//     var PhoneDataDialCode="";
//     this.countryCodeData=CountryISO.India;
//     var phone;
//     if(this.SentData){
// if(this.SentData=="Mail"){
//   console.log("inside mail");
//   valueData=this.data.email;
//   PhoneDataNumber="";
//   PhoneDataDialCode="";
//   this.countryCodeData=CountryISO.India;
// }
// else if(this.SentData=="Phone"){
//   console.log("inside phone");
//   this.countryCodeData=this.data.country;
//   PhoneDataNumber=this.data.phone;
//   PhoneDataDialCode=this.data.countryCode;
//   phone={}
// }
// else{
//   console.log("inside else");
//   valueData="";
//   this.countryCodeData=this.data.country;
//   PhoneDataNumber="";
//   PhoneDataDialCode="";

// }

//     }
    this.ProfileForm = this.fb.group({
      firstName: ["", Validators.required],
      lastName: ["", Validators.required],
      // userName: ["", Validators.required],
      email: ["", [Validators.required, Validators.email]],
      phone: [""],
      // address: this.fb.group({
      //   // <-- the child FormGroup
      //   address: ["", Validators.required],
      //   location: ["", Validators.required],
      //   zipcode: ["", Validators.required]
      // }),
      password: ["", Validators.required],
      cpassword: ["", Validators.required]
    });
  }

  checkConfirm(pass, confirmpass) {
    String(confirmpass.value) == String(pass.value);
    this.confirmError = !(String(confirmpass.value) == String(pass.value))
    return String(confirmpass.value) == String(pass.value)
  }

  // public handleAddressChange(address) {
  //   // Do some stuff
  //   this.ProfileForm.controls.address = address;
  //   console.log(address, "---log");
  //   let lat = address.geometry.location.lat();
  //   let lng = address.geometry.location.lng();
  // }
  public errorHandling = (control: string, error: string) => {
    return this.ProfileForm.controls[control].hasError(error);
  }

  onChange(event) {
    var files = event.srcElement.files;
    this.file = files[0];
    if (event.target.files[0].type.indexOf("image/") == 0) {
    this.loadImage(this.file.type, files);
    }
    else{
      this.commonService.showToasterError("Invalid Image!!!");
    }
  }

  loadImage(mimeType, files) {
    console.log("--lasdjfjalsfdj", files);

    if (mimeType.match(/image\/*/) == null) {
      return;
    }
    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = _event => {
      this.imgURL = reader.result;
      this.profilePic = this.imgURL;
    };
  }


  prepareForm() {
    const controls = this.ProfileForm.controls;
console.log("controls.phone.value.dialCode"+controls.phone.value.dialCode);

const userId = 'user001';
          // this.messagingService.requestPermission(userId);
          // this.messagingService.receiveMessage();
          // this.message = this.messagingService.currentMessage;
    let data = {
      "firstName": controls.firstName.value,
      "lastName": controls.lastName.value,
      // userName: controls.userName.value,
      "email": controls.email.value,
      "password": controls.password.value,
      "phone": controls.phone.value.number,
      "countryCode": controls.phone.value.dialCode,
      "confirmPassword":controls.cpassword.value
      // "country":controls.phone.value.countryCode,
      // "facebookId":"",
      // "isSocialRegister":0,
      // "googleId":"",
      // "deviceType":"web",
	// "deviceId":this.messagingService.token
    };

    let formData = new FormData();
    formData.append("profilePic", this.file);
    formData.append("firstName", controls.firstName.value);
    formData.append("lastName", controls.lastName.value);
    formData.append("email", controls.email.value);
    formData.append("password", controls.password.value);
    formData.append("confirmPassword", controls.cpassword.value);
    formData.append("phone", controls.phone.value.number);
    formData.append("countryCode", controls.phone.value.dialCode);

    this.completeSignup(formData,data);
  }

  completeSignup(_data,dataemail) {
    debugger;
    this.authenticationService.completeRegistration(_data).subscribe(
      (_response: any) => {
        console.log("Response is "+JSON.stringify(_response));
        if(_response.success){
          this.commonService.showToasterSuccess(_response.message);
      
          // this.router.navigate(['/verfication']);
          this.router.navigate(['verfication'], { queryParams: { 'email': dataemail.email }});
        }
        // else{
        //   this.commonService.showToasterError("Server Error");
        // }
        // if (_response.response.success) {
        //   localStorage.setItem(
        //     "token","SEC "+
        //     _response["data"]['authToken']
        //   );
        //   localStorage.setItem(
        //     "loggedInUser",JSON.stringify(
        //     _response["data"])
        //   );
        //   localStorage.removeItem("signUp");
        //   localStorage.removeItem("profileSetup");

        //   // this.router.navigate(["/"]);
        //   if(this.viewIs=="home"){
        //     this.router.navigate(["/"]);
        //   }
        //   else{
        //     this.router.navigate(["/home"]);
        //   }
          
        // } else {
        //   this.commonService.showToasterError(_response.response["message"]);
        // }
      },
      err => {

        console.log("error occured"+JSON.stringify(err));
        if (err.error)
          this.commonService.showToasterError(err.error["message"]);
      }
    );
  }

  movetonext()
  {
    this.router.navigate(['/']);
  }

  testpattern(phone){

    var phonee=phone
var pattern="^[0-9]*$";
return phonee.match(pattern);
  }

  onSumbit() {
    debugger
    const controls = this.ProfileForm.controls;
    this.submitted=true;
    /** check form */
    if (this.ProfileForm.invalid) {
      // this.commonService.showToasterError('Please fill required Field appropriately');
      if(this.ProfileForm.controls['phone'].invalid)
      {
        this.commonService.showToasterError('Phone number is not valid ');
      }
      if(this.ProfileForm.controls['email'].invalid)
      {
        this.commonService.showToasterError('Email is not valid ');
      }
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );
      return;
      
    } 
    // else if(!this.profilePic) {
    //   this.commonService.showToasterError('Please add your profile picture');
    //   return
    // } 
    else {
if(controls["phone"] && controls["phone"].value){
  debugger;
  
  var testvalue=this.testpattern(controls["phone"].value.number);
  if(testvalue){

    if(controls["password"].value==controls["cpassword"].value){
      this.prepareForm();
    }
    else{
      this.commonService.showToasterError('Password and confirm password does not match ');
    }
  
  }
  else{
    this.commonService.showToasterError('Phone number is not valid ');
   }
}
else{
  this.commonService.showToasterError('Phone number is not valid ');
}
     
    }
  }

  goTologin() {
    this.router.navigate(['login']);
  }


}
