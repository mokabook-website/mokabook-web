import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { StringifyOptions } from 'querystring';
import { AuthenticationService } from '../services/auth.service';
import { CommonService } from '../services/common.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { validateHorizontalPosition } from '@angular/cdk/overlay';

@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.scss']
})
export class TermsComponent implements OnInit {

  name:any;
  data:any;
  cname:string = null;
  cemail:string = null;
  submitted:boolean = false;
  cdescription:string = null;
  slugname:any;
  description:any;
  profileForm = new FormGroup({
    name: new FormControl('',Validators.required),
    email: new FormControl('',Validators.required),
    description: new FormControl('',Validators.required)
  });
  constructor(private route:ActivatedRoute,private api :AuthenticationService ,private router:Router,private commanSrv:CommonService) { }

  ngOnInit() {

    
    this.route.queryParams.subscribe(params => {this.name = params['check']});
   this.gettermsPolicy(this.name)
   this.api.$termscond.subscribe((res)=>
   {
     this.gettermsPolicy(res);
   })
  }

  public errorHandling = (control: string, error: string) => {
    return this.profileForm.controls[control].hasError(error);
  }


  gettermsPolicy(value)
  {
    this.api.gettermsandpolicy().subscribe((res)=>{
      this.data = res.data.filter(id=>id.slugName == value)
      this.slugname = this.data[0].title
      this.description = this.data[0].description;
      console.log(this.data)
    })
  }

  sendContactUs()
  {
    this.submitted = true;
   const data =
   {
   "name":this.cname,
   "email":this.cemail,
   "message":this.cdescription,
   } 
  console.log(this.profileForm.valid);
  if(this.profileForm.valid)
  {
    this.api.contactUs(data).subscribe((res:any)=>
    {
     if(res.success)
     {
      this.submitted = false;
       this.commanSrv.showToasterSuccess("Thanks for Contacting Us!")
       this.profileForm.reset();
     }
    })
  }
  else
  {
    //this.commanSrv.showToasterError("Please fill all the mandatory(*) fields")
  }
  }

}
