import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.scss']
})
export class WishlistComponent implements OnInit {
  favproduct:any;
  imgurl:any;
  constructor(public authenticationService: AuthenticationService,public router:Router) { }

  ngOnInit() {
    this.imgurl=this.authenticationService.imageUrl;

    this.authenticationService.favoriteproduct().subscribe((res:any)=>
    {
      this.favproduct = res.data.list;
      console.log("favlist", this.favproduct);
    })
  }


  removefav(id)
  {
    this.authenticationService.postfavlist(id).subscribe((res:any)=>
    {
      if(res.success)
      {
        this.authenticationService.favoriteproduct().subscribe((res:any)=>
        {
          this.favproduct = res.data.list;
        })
      }
    })
  }
  gotohomepage()
  {
    this.router.navigate(['/home']);
  }
  gotoprod_page(id)
  {
    this.router.navigate(['productdetail'], { queryParams: { 'id': id }});
  }
}
