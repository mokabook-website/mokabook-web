import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './header/header.component';
import { SignupComponent } from './signup/signup.component';
import { VerficationComponent } from './verfication/verfication.component';
import { ForgetpasswordComponent } from './forgetpassword/forgetpassword.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { OrderdetailComponent } from './orderdetail/orderdetail.component';
import { CategoriesdetailComponent } from './categoriesdetail/categoriesdetail.component';
import { CategoriesComponent } from './categories/categories.component';
import { CartComponent } from './cart/cart.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { ProductdetailComponent } from './productdetail/productdetail.component';
import { FooterComponent } from './footer/footer.component';
import { WishlistComponent } from './wishlist/wishlist.component';
import { ForgotPasswordEmailComponent } from './forgot-password-email/forgot-password-email.component';
import { BannerpageComponent } from './bannerpage/bannerpage.component';
import { NotfoundPageComponent } from './notfound-page/notfound-page.component';
import { HeaderpageComponent } from './headerpage/headerpage.component';
import { SubcategeoryComponent } from './subcategeory/subcategeory.component';
import { TermsComponent } from './terms/terms.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: HomeComponent
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'header',
    component: HeaderComponent
  },
  {
    path: 'signup',
    component: SignupComponent
  },
  {
    path: 'verfication',
    component: VerficationComponent
  },
  {
    path: 'forgetpassword',
    component: ForgetpasswordComponent
  },
   {
    path: 'forgetpasswordemail',
    component: ForgotPasswordEmailComponent
  },

  
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'profile',
    component: ProfileComponent
  },
  {
    path: 'orderdetail',
    component: OrderdetailComponent
  },
  {
    path: 'categoriesdetail',
    component: CategoriesdetailComponent
  },
  {
    path: 'categories',
    component: CategoriesComponent
  },

  {
    path: 'categories/category/:subcategory',
    component: CategoriesComponent
  },
  {
    path: 'cart',
    component: CartComponent
  },
  {
    path: 'checkout',
    component: CheckoutComponent
  },
  {
    path: 'productdetail',
    component: ProductdetailComponent
  },
  {
    path: 'footer',
    component: FooterComponent
  },
  {
    path: 'wishlist',
    component: WishlistComponent
  },
  {
    path: 'banner',
    component: BannerpageComponent
  },
  {
    path:'notfound',
    component: NotfoundPageComponent
  }
,
{
    path:'headerpage',
    component : HeaderpageComponent
},
{
  path: 'subcategrory',
  component: SubcategeoryComponent
},
{
  path: 'terms&cond',
  component: TermsComponent
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{onSameUrlNavigation: 'reload',useHash: true,scrollPositionRestoration: 'top'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
