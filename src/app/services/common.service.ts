import { Injectable  } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject, Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  messageSource: BehaviorSubject<any>;
  eventmessageSource: BehaviorSubject<boolean>;

  // currentMessage = this.messageSource.asObservable();

  constructor(private toastr: ToastrService, private http: HttpClient) {
    this.messageSource = new BehaviorSubject<any>([]);
    this.eventmessageSource = new BehaviorSubject<boolean>(false);
  }

  /** Common services */

  showToasterSuccess(message: string) {
    this.toastr.success(message);
  }

  showToasterError(message: string) {
    this.toastr.error(message);
  }

  getData(): Observable<any> {
    return this.messageSource.asObservable();
  }

  passData(data) {
    this.messageSource.next(data);
  }

  changeEvent(data) {
    this.eventmessageSource.next(data);
  }

  geteventData(): Observable<boolean> {
    return this.eventmessageSource.asObservable();
  }
}
