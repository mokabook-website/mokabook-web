import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as io from 'socket.io-client';
import { CommonService } from './common.service';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/auth.service';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { catchError, map, tap } from 'rxjs/internal/operators';
@Injectable({
    providedIn: 'root'
})
export class NotificationService {
    socket;
    constructor(private authSrv: AuthenticationService){}
    setupSocketConnection() {
        this.socket = io(this.authSrv.imageUrl);
        console.log("socket connected");
    }
}