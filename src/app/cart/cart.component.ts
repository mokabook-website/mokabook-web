import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/auth.service';
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  imgUrl:any;
  checkdata:any;
  countcart = 0
  i:any;
  amount:any;
  price_total:any;
  public cartdata:any;
  pricedata:any;
  constructor(private router: Router,public authservice:AuthenticationService) 
  {

  }

  ngOnInit() 
  {
    this.imgUrl = this.authservice.imageUrl;
    this.authservice.getcartlist().subscribe((res:any)=>{
     this.cartdata = res.data;
   //  console.log("cart",this.cartdata);
     this.getcartcount(res.data.length);
   })

  }
  goTocheckout()
  {
    this.router.navigate(['/checkout']);
  }

  gotohome()
  {
    this.router.navigate(['/home']);
  }

  removecart(id)
  {
    this.authservice.removecarddata(id).subscribe((data:any)=>{
    //  console.log("remove",data)
      if(data.status)
      {
        this.authservice.getcartlist().subscribe((res:any)=>{
          this.cartdata = res.data;
      //    console.log("data deleted",res.data)
          this.getcartcount(res.data.length);
        });
      }
    });
  }

  subquantity(cartdata)
  {
    debugger
  //  console.log("cart",cartdata);
    this.i = cartdata.quantity - 1;
    if(this.i != 0)
    {
      this.amount = cartdata.amount - cartdata.productId.price
      this.authservice.updatelist(cartdata,this.i,cartdata.productId.price).subscribe((res:any)=>
      {
        if(res.status)
        {
          this.authservice.getcartlist().subscribe((res:any)=>{
            this.cartdata = res.data; 
            this.getcartcount(res.data.length);
          });  
        }
      })
    }
  }

  addquantity(id)
  {
    debugger
    this.authservice.getcartlist().subscribe((res:any)=>
    {
      this.checkdata = res.data.filter(i=>i._id == id)[0];
     // console.log("id",this.checkdata) 
      this.i = this.checkdata.quantity + 1;
      if(this.checkdata.productId.purchaseQuantity >= this.i){    
      this.amount = this.checkdata.amount + this.checkdata.productId.price
      this.pricedata = this.checkdata.productId.price;
      if(res.status)
      {
          
           this.authservice.updatelist(this.checkdata,this.i,this.pricedata).subscribe((res:any)=>
           {
             if(res.status)
             {
              this.authservice.getcartlist().subscribe((res:any)=>
              {
      //          console.log("cart",res);
                 this.cartdata = res.data; 
                 this.getcartcount(res.data.length);
              });  
             }
           })
      }
    }
    });
    
  }


  getcartcount(count)
  {
    //console.log(count);
    this.countcart = 0;
    this.price_total = 0;
    for(var i=0;i<count;i++)
    {
       this.countcart =  this.cartdata[i].quantity + this.countcart;
       //console.log("co",this.countcart)
       this.price_total = ((this.cartdata[i].productId.price * this.cartdata[i].quantity)-(this.cartdata[i].productId.price * this.cartdata[i].quantity * (this.cartdata[i].productId.discount)/100)) + this.price_total;
       //console.log("ros",this.price_total);
    }
    this.authservice.getcartlength(this.countcart);

  }

}
