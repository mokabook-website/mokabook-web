import { Component, OnInit,Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { AuthenticationService } from '../services/auth.service';
import { Router } from '@angular/router';
import { CommonService } from '../services/common.service';
export interface DialogData {
 
}

@Component({
  selector: 'app-canceldialogbox',
  templateUrl: './canceldialogbox.component.html',
  styleUrls: ['./canceldialogbox.component.scss']
})
export class CanceldialogboxComponent {
 history_id:any;
  reason:any;
  constructor(
    public dialogRef: MatDialogRef<CanceldialogboxComponent>, private commonService: CommonService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,private authservice:AuthenticationService,private router: Router) 
    {
      debugger;
      this.authservice.historyid.subscribe((res:any)=>{
        this.history_id = res.id;
        console.log("historyskdugfysdugfg",this.history_id);
      })
    }

  onNoClick(): void {
    this.dialogRef.close();
  }

  setdata()
  {
    const datacheck = {
      "id":this.history_id,
      "cancelReason":this.reason

    };
    if(this.reason == undefined)
    {
      this.commonService.showToasterError("Please enter your reason!");
    }
    this.authservice.cancelorder(datacheck).subscribe((res:any)=>
    {
      console.log("reson",this.reason);
      if(res.success)
      {
       this.router.navigate(['/profile']);
      }
    })
   console.log("reason",this.reason,);
  }

}
