import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/auth.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-headerpage',
  templateUrl: './headerpage.component.html',
  styleUrls: ['./headerpage.component.scss']
})
export class HeaderpageComponent implements OnInit {

  public  subcategorydataid :any;
  public cate_id:any;
  public subcat_id:any;
  public imgurl:any;
  public productdata:any;
  constructor(private authservice:AuthenticationService,private router:Router )
  { 
    this.imgurl=this.authservice.imageUrl;
      this.authservice.historyid.subscribe((res:any)=>
      {
       this.cate_id = res;
    this.authservice.getcategoryTablist(res).subscribe((res:any)=>
    {
      //  console.log(res);
        this.subcategorydataid = res.data[2].value[0]._id
        if(res.success)
        {
          const data =
          {
            "id": this.subcategorydataid,
            "page": 1,
            "count": 10,
            "category": this.cate_id,
            "isRecommended":false,
            "isFeatured":false
          }
          this.authservice.getprouctdata(data).subscribe((res:any)=>
          {
           // console.log("ggggggggggggggggggggggggggg",res);
           this.productdata = res.data;
           console.log("checkdata",this.productdata);
          })
        }
    })

      });
  }

  ngOnInit() {
  }

  gotoproductDetails(id)
{
  this.router.navigate(['productdetail'], { queryParams: { 'id': id }});
}

}
